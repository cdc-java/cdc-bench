package cdc.bench.io.files;

public class ChecksumAnalyzer extends Analyzer {
   private int checksum = 0;

   @Override
   public void processBegin() {
      super.processBegin();
      checksum = 0;
   }

   @Override
   public void processData(byte b) {
      super.processData(b);
      checksum += b;
   }

   public final int getChecksum() {
      return checksum;
   }
}