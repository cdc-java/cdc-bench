package cdc.bench.io.files;

public enum AccessMode {
   READ,
   WRITE
}