package cdc.bench.io.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.time.Chronometer;

public class Test02 {
    private static final Logger LOGGER = LogManager.getLogger(Test02.class);
    private static final Chronometer CHRONO = new Chronometer();
    private static final int ITERATIONS = 5;
    private static final double MEG = (Math.pow(1024, 2));
    private static final int RECORD_COUNT = 4_000_000;
    private static final String RECORD = "Help I am trapped in a fortune cookie factory\n";
    private static final int RECSIZE = RECORD.getBytes().length;

    public static void main(String[] args) throws Exception {
        final List<String> records = new ArrayList<>(RECORD_COUNT);
        int size = 0;
        for (int i = 0; i < RECORD_COUNT; i++) {
            records.add(RECORD);
            size += RECSIZE;
        }
        LOGGER.info("{} 'records'", records.size());
        LOGGER.info("{} MB", size / MEG);

        for (int i = 0; i < ITERATIONS; i++) {
            LOGGER.info("Iteration {}", i);

            writeRaw(records);
            writeBuffered(records, 8192);
            writeBuffered(records, (int) MEG);
            writeBuffered(records, 4 * (int) MEG);
        }
    }

    private static void writeRaw(List<String> records) throws IOException {
        final File file = File.createTempFile("foo", ".txt");
        try (final FileWriter writer = new FileWriter(file)) {
            LOGGER.info("Writing raw... ");
            write(records, writer);
        } finally {
            // comment this out if you want to inspect the files afterward
            Files.delete(file.toPath());
        }
    }

    private static void writeBuffered(List<String> records,
                                      int bufSize) throws IOException {
        final File file = File.createTempFile("foo", ".txt");
        try (final FileWriter writer = new FileWriter(file)) {
            final BufferedWriter bufferedWriter = new BufferedWriter(writer, bufSize);

            LOGGER.info("Writing buffered (buffer size: {}) ...", bufSize);
            write(records, bufferedWriter);
        } finally {
            // comment this out if you want to inspect the files afterward
            Files.delete(file.toPath());
        }
    }

    private static void write(List<String> records,
                              Writer writer) throws IOException {
        CHRONO.start();
        for (final String r : records) {
            writer.write(r);
        }
        writer.flush();
        writer.close();
        CHRONO.suspend();
        LOGGER.info("{}", CHRONO);
    }
}