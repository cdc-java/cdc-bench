package cdc.bench.io.files;

import cdc.util.time.Chronometer;

public abstract class Analyzer {
    private final Chronometer chrono = new Chronometer();
    private long count = 0;

    public void processBegin() {
        chrono.start();
        setCount(0);
    }

    public void processData(byte b) {
        setCount(getCount() + 1);
    }

    public void processEnd() {
        chrono.suspend();
    }

    public final Chronometer getChronometer() {
        return chrono;
    }

    public final long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}