package cdc.bench.io.files;

import cdc.util.lang.UnexpectedValueException;

public final class TestCaseFactory {
    private TestCaseFactory() {
    }

    public static TestCase createTestCase(AccessMethod method) {
        switch (method) {
        case BUFFERED_CHANNEL:
            return new BufferedChannelFileTestCase();
        case BUFFERED_STREAM:
            return new BufferedStreamFileTestCase();
        case MEMORY_MAPPED:
            return new MemoryMappedFileTestCase();
        case RANDOM_ACCESS:
            return new RandomAccessFileTestCase();
        default:
            throw new UnexpectedValueException(method);
        }
    }
}