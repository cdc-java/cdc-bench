package cdc.bench.io.files;

import static java.nio.channels.FileChannel.MapMode.READ_ONLY;
import static java.nio.channels.FileChannel.MapMode.READ_WRITE;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import cdc.util.lang.ExceptionWrapper;

public class MemoryMappedFileTestCase extends TestCase {

    public MemoryMappedFileTestCase() {
        super("MemoryMappedFile");
    }

    @Override
    public double testRead(File file,
                           Settings settings,
                           Analyzer analyzer) {
        analyzer.processBegin();
        try (final RandomAccessFile raf = new RandomAccessFile(file, "r");
                final FileChannel channel = raf.getChannel()) {
            MappedByteBuffer buffer = channel.map(READ_ONLY, 0, Math.min(channel.size(), Integer.MAX_VALUE));

            for (long i = 0; i < settings.getFileSize(); i++) {
                if (!buffer.hasRemaining()) {
                    buffer = channel.map(READ_WRITE, i, Math.min(channel.size() - i, Integer.MAX_VALUE));
                }
                final byte b = buffer.get();
                analyzer.processData(b);
            }
            analyzer.processEnd();
            return settings.getFileSize();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public double testWrite(File file,
                            Settings settings,
                            Analyzer analyzer) {
        analyzer.processBegin();
        try (final RandomAccessFile raf = new RandomAccessFile(file, "rw");
                final FileChannel channel = raf.getChannel()) {
            MappedByteBuffer buffer = channel.map(READ_WRITE, 0, Math.min(channel.size(), Integer.MAX_VALUE));
            final byte[] chunk = settings.getChunk();

            int i = 0;
            for (int chunkIndex = 0; chunkIndex < settings.getChunkCount(); chunkIndex++) {
                for (int index = 0; index < chunk.length; index++) {
                    if (!buffer.hasRemaining()) {
                        buffer = channel.map(READ_WRITE, i, Math.min(channel.size() - i, Integer.MAX_VALUE));
                    }
                    final byte b = chunk[index];
                    analyzer.processData(b);
                    buffer.put(b);
                    i++;
                }
            }
            analyzer.processEnd();
            return settings.getFileSize();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }
}