package cdc.bench.io.files;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 0, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, time = 10, timeUnit = TimeUnit.MILLISECONDS)
public class FileAccessBenchmark {

    @Param({ "BUFFERED_CHANNEL", "BUFFERED_STREAM", "MEMORY_MAPPED", "RANDOM_ACCESS" })
    private AccessMethod method;

    @Param({ "WRITE", "READ" })
    private AccessMode mode;

    @Param({ "1", "2", "4", "8", "16", "32", "64", "128", "256", "512", "1024", "2048", "4096", "8192", "16384", "32768" })
    private int chunkSize;

    @Param({ "1048576" })
    private int fileSize;

    private final ChecksumAnalyzer analyzer = new ChecksumAnalyzer();

    private Settings settings;
    private TestCase tc;

    @Setup
    public void setup() {
        settings = new Settings(chunkSize, fileSize / chunkSize);
        tc = TestCaseFactory.createTestCase(method);
    }

    @Benchmark
    public double bench() {
        return tc.execute(new File("out.txt"), settings, analyzer, mode);
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(FileAccessBenchmark.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", FileAccessBenchmark.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}