package cdc.bench.io.files;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import cdc.util.lang.ExceptionWrapper;

public class BufferedStreamFileTestCase extends TestCase {

    public BufferedStreamFileTestCase() {
        super("BufferedStreamFile");
    }

    @Override
    public double testRead(File file,
                           Settings settings,
                           Analyzer analyzer) {
        analyzer.processBegin();
        try (final InputStream in = new BufferedInputStream(new FileInputStream(file))) {
            int i;
            while (-1 != (i = in.read())) {
                analyzer.processData((byte) i);
            }
            analyzer.processEnd();
            return settings.getFileSize();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public double testWrite(File file,
                            Settings settings,
                            Analyzer analyzer) {
        analyzer.processBegin();
        try (final OutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
            final byte[] chunk = settings.getChunk();
            for (int chunkIndex = 0; chunkIndex < settings.getChunkCount(); chunkIndex++) {
                for (int index = 0; index < chunk.length; index++) {
                    final byte b = chunk[index];
                    analyzer.processData(b);
                    out.write(b);
                }
            }
            analyzer.processEnd();
            return settings.getFileSize();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }
}