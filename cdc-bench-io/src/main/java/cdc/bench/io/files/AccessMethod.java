package cdc.bench.io.files;

public enum AccessMethod {
    BUFFERED_CHANNEL,
    BUFFERED_STREAM,
    MEMORY_MAPPED,
    RANDOM_ACCESS
}