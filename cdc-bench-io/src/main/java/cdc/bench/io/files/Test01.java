package cdc.bench.io.files;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.time.Chronometer;

public class Test01 {
    private static final Logger LOGGER = LogManager.getLogger(Test01.class);

    public static void main(String[] args) {
        final Chronometer chrono = new Chronometer();
        final byte[] buffer = "Help I am trapped in a fortune cookie factory\n".getBytes();
        final long numberOfLines = 400_000;

        LOGGER.info("start");
        chrono.start();
        try (final RandomAccessFile raf = new RandomAccessFile("target/textfile.txt", "rw");
                final FileChannel rwChannel = raf.getChannel()) {
            final ByteBuffer rwBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, buffer.length * numberOfLines);
            for (int i = 0; i < numberOfLines; i++) {
                rwBuf.put(buffer);
            }
            chrono.suspend();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        LOGGER.info("done ({})", chrono);
    }
}