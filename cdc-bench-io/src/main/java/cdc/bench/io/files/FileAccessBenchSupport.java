package cdc.bench.io.files;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import cdc.bench.support.ChunkSupport;
import cdc.util.cli.AbstractMainSupport;

public final class FileAccessBenchSupport {
    private FileAccessBenchSupport() {
    }

    public static final String MODE = "mode";
    public static final String METHOD = "method";
    public static final String TMP_FILE = "tmp-file";

    public static void addSpecificOptions(Options options) {
        ChunkSupport.addSpecificOptions(options);

        options.addOption(Option.builder()
                                .longOpt(MODE)
                                .desc("Should a READ or WRITE test be run? (default WRITE).")
                                .argName("READ|WRITE")
                                .hasArg()
                                .build());

        options.addOption(Option.builder()
                                .longOpt(METHOD)
                                .desc("Method used to access file: RANDOM_ACCESS|BUFFERED_STREAM|BUFFERED_CHANNEL|MAPPED_MEMORY. (default RANDOM_ACCESS.).")
                                .hasArg()
                                .build());

        options.addOption(Option.builder()
                                .longOpt(TMP_FILE)
                                .desc("Name of the file to read or write (default ./tmp.txt).")
                                .hasArg()
                                .build());
    }

    public static void analyze(CommandLine cl,
                               FileAccessBench.MainArgs margs) throws ParseException {
        ChunkSupport.analyze(cl, margs);

        margs.tmpFile = AbstractMainSupport.getValueAsFile(cl, TMP_FILE, new File("./tmp.txt"));
        margs.mode = AbstractMainSupport.getValueAsEnum(cl, MODE, AccessMode.class, AccessMode.WRITE);
        margs.method = AbstractMainSupport.getValueAsEnum(cl, METHOD, AccessMethod.class, AccessMethod.RANDOM_ACCESS);
    }
}