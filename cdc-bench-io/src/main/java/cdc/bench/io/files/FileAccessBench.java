package cdc.bench.io.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.bench.support.BasicBenchExecutor;
import cdc.bench.support.Bench;
import cdc.bench.support.BenchData;
import cdc.bench.support.ChunkSupport;
import cdc.bench.support.Config;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.MainResult;
import cdc.util.lang.UnexpectedValueException;

public class FileAccessBench implements Bench {
    protected static final Logger LOGGER = LogManager.getLogger(FileAccessBench.class);

    public static class MainArgs extends ChunkSupport.MainArgs {
        public AccessMode mode;
        public AccessMethod method;
        public File tmpFile;
    }

    private final ChecksumAnalyzer analyzer = new ChecksumAnalyzer();
    private final MainArgs margs;
    private final Settings settings;
    private final TestCase tc;

    public FileAccessBench(MainArgs margs) {
        this.margs = margs;
        settings = new Settings(margs.chunksSize, margs.chunksCount);
        switch (margs.method) {
        case BUFFERED_CHANNEL:
            tc = new BufferedChannelFileTestCase();
            break;
        case BUFFERED_STREAM:
            tc = new BufferedStreamFileTestCase();
            break;
        case MEMORY_MAPPED:
            tc = new MemoryMappedFileTestCase();
            break;
        case RANDOM_ACCESS:
            tc = new RandomAccessFileTestCase();
            break;
        default:
            throw new UnexpectedValueException(margs.method);
        }
    }

    @Override
    public String getQuantityUnitName() {
        return "KiB";
    }

    @Override
    public double getQuantityUnit() {
        return 1024.0;
    }

    @Override
    public void startBench(BenchData data,
                           List<String> values) {
        if (margs.mode == AccessMode.READ) {
            // Create the file that will be read
            tc.execute(margs.tmpFile, settings, analyzer, AccessMode.WRITE);
        }

        values.add("Mode");
        values.add("File");
        values.add("Method");
        values.add("Chunk Size");
        values.add("Chunks Count");
    }

    @Override
    public void startTest(BenchData data) {
        // Ignore
    }

    @Override
    public void startSample(BenchData data) {
        if (margs.mode == AccessMode.WRITE) {
            try {
                Files.delete(margs.tmpFile.toPath());
            } catch (final IOException e) {
                LOGGER.catching(e);
            }
        }
    }

    @Override
    public double sample(BenchData data) {
        return tc.execute(margs.tmpFile, settings, analyzer, margs.mode);
    }

    @Override
    public void endSample(BenchData data) {
        // Ignore
    }

    @Override
    public void endTest(BenchData data,
                        List<String> values) {
        values.add(margs.mode.name());
        values.add(margs.tmpFile.getPath());
        values.add(margs.method.name());
        values.add(margs.chunksSize + "");
        values.add(margs.chunksCount + "");
    }

    @Override
    public void endBench(BenchData data) {
        // Ignore
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        public MainSupport() {
            super(FileAccessBench.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            FileAccessBenchSupport.addSpecificOptions(options);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            FileAccessBenchSupport.analyze(cl, margs);
            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            final FileAccessBench monitor = new FileAccessBench(margs);
            final BasicBenchExecutor executor = new BasicBenchExecutor(monitor, margs);
            executor.run();
            return null;
        }
    }
}