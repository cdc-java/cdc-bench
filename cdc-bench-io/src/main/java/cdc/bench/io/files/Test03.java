package cdc.bench.io.files;

import static java.lang.Integer.MAX_VALUE;
import static java.nio.channels.FileChannel.MapMode.READ_ONLY;
import static java.nio.channels.FileChannel.MapMode.READ_WRITE;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;

import cdc.util.lang.ExceptionWrapper;

public final class Test03 {
    private static final Logger LOGGER = LogManager.getLogger(Test03.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    public static final int PAGE_SIZE = 1024 * 4;
    public static final long FILE_SIZE = PAGE_SIZE * 100L * 1000L;
    public static final String FILE_NAME = "target/test.dat";
    private static final byte[] BLANK_PAGE = new byte[PAGE_SIZE];

    public static void main(final String[] arg) throws Exception {
        preallocateTestFile(FILE_NAME);

        for (final PerfTestCase testCase : testCases) {
            for (int i = 0; i < 5; i++) {
                final long writeDurationMs = testCase.test(PerfTestCase.Type.WRITE, FILE_NAME);

                final long readDurationMs = testCase.test(PerfTestCase.Type.READ, FILE_NAME);

                final long bytesReadPerSec = (FILE_SIZE * 1000L) / readDurationMs;
                final long bytesWrittenPerSec = (FILE_SIZE * 1000L) / writeDurationMs;

                OUT.format("%s\twrite=%,d\tread=%,d bytes/sec%n",
                           testCase.getName(),
                           bytesWrittenPerSec,
                           bytesReadPerSec);
            }
        }
        deleteFile(FILE_NAME);
    }

    private static void preallocateTestFile(final String fileName) throws IOException {
        try (final RandomAccessFile file = new RandomAccessFile(fileName, "rw")) {
            for (long i = 0; i < FILE_SIZE; i += PAGE_SIZE) {
                file.write(BLANK_PAGE, 0, PAGE_SIZE);
            }
        }
    }

    private static void deleteFile(final String testFileName) throws IOException {
        final File file = new File(testFileName);
        try {
            Files.delete(file.toPath());
        } catch (final IOException e) {
            OUT.println("Failed to delete test file=" + testFileName);
            OUT.println("Windows does not allow mapped files to be deleted.");
            throw e;
        }
    }

    public abstract static class PerfTestCase {
        public enum Type {
            READ,
            WRITE
        }

        private final String name;
        private int checkSum;

        protected PerfTestCase(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public long test(final Type type,
                         final String fileName) {
            final long start = System.currentTimeMillis();

            try {
                if (type == Type.WRITE) {
                    checkSum = testWrite(fileName);
                } else {
                    final int cs = testRead(fileName);
                    if (cs != this.checkSum) {
                        final String msg = getName() +
                                " expected=" + this.checkSum +
                                " got=" + cs;
                        throw new IllegalStateException(msg);
                    }
                }
            } catch (final Exception ex) {
                ex.printStackTrace();
            }

            return System.currentTimeMillis() - start;
        }

        public abstract int testWrite(final String fileName);

        public abstract int testRead(final String fileName);
    }

    private static PerfTestCase[] testCases = {
            new PerfTestCase("RandomAccessFile") {
                @Override
                public int testWrite(final String fileName) {
                    try (final RandomAccessFile file = new RandomAccessFile(fileName, "rw")) {
                        final byte[] buffer = new byte[PAGE_SIZE];
                        int pos = 0;
                        int checkSum = 0;
                        for (long i = 0; i < FILE_SIZE; i++) {
                            final byte b = (byte) i;
                            checkSum += b;

                            buffer[pos++] = b;
                            if (PAGE_SIZE == pos) {
                                file.write(buffer, 0, PAGE_SIZE);
                                pos = 0;
                            }
                        }
                        return checkSum;
                    } catch (final IOException e) {
                        throw new ExceptionWrapper(e);
                    }
                }

                @Override
                public int testRead(final String fileName) {
                    try (final RandomAccessFile file = new RandomAccessFile(fileName, "r")) {
                        final byte[] buffer = new byte[PAGE_SIZE];
                        int checkSum = 0;
                        int bytesRead;
                        while (-1 != (bytesRead = file.read(buffer))) {
                            for (int i = 0; i < bytesRead; i++) {
                                checkSum += buffer[i];
                            }
                        }
                        return checkSum;
                    } catch (final IOException e) {
                        throw new ExceptionWrapper(e);
                    }
                }
            },

            new PerfTestCase("BufferedStreamFile") {
                @Override
                public int testWrite(final String fileName) {
                    try (final OutputStream out = new BufferedOutputStream(new FileOutputStream(fileName))) {
                        int checkSum = 0;
                        for (long i = 0; i < FILE_SIZE; i++) {
                            final byte b = (byte) i;
                            checkSum += b;
                            out.write(b);
                        }
                        return checkSum;
                    } catch (final IOException e) {
                        throw new ExceptionWrapper(e);
                    }
                }

                @Override
                public int testRead(final String fileName) {
                    try (final InputStream in = new BufferedInputStream(new FileInputStream(fileName))) {
                        int checkSum = 0;
                        int b;
                        while (-1 != (b = in.read())) {
                            checkSum += (byte) b;
                        }
                        return checkSum;
                    } catch (final IOException e) {
                        throw new ExceptionWrapper(e);
                    }
                }
            },

            new PerfTestCase("BufferedChannelFile") {
                @Override
                public int testWrite(final String fileName) {
                    try (final RandomAccessFile raf = new RandomAccessFile(fileName, "rw");
                            final FileChannel channel = raf.getChannel()) {
                        final ByteBuffer buffer = ByteBuffer.allocate(PAGE_SIZE);
                        int checkSum = 0;
                        for (long i = 0; i < FILE_SIZE; i++) {
                            final byte b = (byte) i;
                            checkSum += b;
                            buffer.put(b);

                            if (!buffer.hasRemaining()) {
                                buffer.flip();
                                channel.write(buffer);
                                buffer.clear();
                            }
                        }
                        return checkSum;
                    } catch (final IOException e) {
                        throw new ExceptionWrapper(e);
                    }
                }

                @Override
                public int testRead(final String fileName) {
                    try (final RandomAccessFile raf = new RandomAccessFile(fileName, "rw")) {
                        final FileChannel channel = raf.getChannel();
                        final ByteBuffer buffer = ByteBuffer.allocate(PAGE_SIZE);
                        int checkSum = 0;
                        while (-1 != (channel.read(buffer))) {
                            buffer.flip();
                            while (buffer.hasRemaining()) {
                                checkSum += buffer.get();
                            }
                            buffer.clear();
                        }
                        return checkSum;
                    } catch (final IOException e) {
                        throw new ExceptionWrapper(e);
                    }
                }
            },

            new PerfTestCase("MemoryMappedFile") {
                @Override
                public int testWrite(final String fileName) {
                    try (final RandomAccessFile raf = new RandomAccessFile(fileName, "rw");
                            final FileChannel channel = raf.getChannel()) {
                        MappedByteBuffer buffer = channel.map(READ_WRITE, 0, Math.min(channel.size(), MAX_VALUE));
                        int checkSum = 0;
                        for (long i = 0; i < FILE_SIZE; i++) {
                            if (!buffer.hasRemaining()) {
                                buffer = channel.map(READ_WRITE, i, Math.min(channel.size() - i, MAX_VALUE));
                            }
                            final byte b = (byte) i;
                            checkSum += b;
                            buffer.put(b);
                        }
                        return checkSum;
                    } catch (final IOException e) {
                        throw new ExceptionWrapper(e);
                    }
                }

                @Override
                public int testRead(final String fileName) {
                    try (final RandomAccessFile raf = new RandomAccessFile(fileName, "rw");
                            final FileChannel channel = raf.getChannel()) {
                        MappedByteBuffer buffer = channel.map(READ_ONLY, 0, Math.min(channel.size(), MAX_VALUE));
                        int checkSum = 0;
                        for (long i = 0; i < FILE_SIZE; i++) {
                            if (!buffer.hasRemaining()) {
                                buffer = channel.map(READ_WRITE, i, Math.min(channel.size() - i, MAX_VALUE));
                            }
                            checkSum += buffer.get();
                        }
                        return checkSum;
                    } catch (final IOException e) {
                        throw new ExceptionWrapper(e);
                    }
                }
            } };
}