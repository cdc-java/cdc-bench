package cdc.bench.io.files.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import cdc.bench.io.files.AccessMethod;
import cdc.bench.io.files.AccessMode;
import cdc.bench.io.files.FileAccessBench;
import cdc.bench.support.checks.CheckResult.Status;
import cdc.bench.support.checks.DoubleChecker;
import cdc.bench.support.checks.IntegerChecker;
import cdc.bench.support.ui.swing.CheckedTextField;
import cdc.bench.support.ui.swing.EnumComboBoxModel;
import cdc.bench.support.ui.swing.SwingBenchExecutor;

public class FileAccessBenchPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    protected final FileAccessBench.MainArgs margs;
    protected final EnumComboBoxModel<AccessMode> modeModel = new EnumComboBoxModel<>(AccessMode.class);
    private final JComboBox<AccessMode> wMode = new JComboBox<>(modeModel);
    protected final EnumComboBoxModel<AccessMethod> methodModel = new EnumComboBoxModel<>(AccessMethod.class);
    private final JComboBox<AccessMethod> wMethod = new JComboBox<>(methodModel);
    private final CheckedTextField wChunkSize = new CheckedTextField(IntegerChecker.POS_INSTANCE);
    private final CheckedTextField wChunksCount = new CheckedTextField(IntegerChecker.NAT_INSTANCE);
    private final CheckedTextField wDelay = new CheckedTextField(DoubleChecker.POS_INSTANCE);
    private final CheckedTextField wSamples = new CheckedTextField(IntegerChecker.POS_INSTANCE);
    private final CheckedTextField wTests = new CheckedTextField(IntegerChecker.INT_INSTANCE);
    private final JButton wStartStop = new JButton("Start");
    protected boolean running = false;
    private MonitorWorker worker;

    private final DocumentListener documentListener = new DocumentListener() {
        @Override
        public void removeUpdate(DocumentEvent e) {
            refresh();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            refresh();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            refresh();
        }
    };

    private void add(int gridy,
                     String label,
                     JComponent w) {
        if (label != null) {
            final JLabel wLabel = new JLabel(label);
            final GridBagConstraints gbc = new GridBagConstraints();
            gbc.insets = new Insets(5, 5, 5, 5);
            gbc.anchor = GridBagConstraints.LINE_START;
            gbc.weightx = 0.0;
            gbc.weighty = 0.0;
            gbc.gridx = 0;
            gbc.gridy = gridy;
            add(wLabel, gbc);
        }
        final GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 0, 5, 5);
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.gridx = 1;
        gbc.gridy = gridy;
        add(w, gbc);
    }

    public FileAccessBenchPanel(FileAccessBench.MainArgs margs) {
        this.margs = margs;

        final GridBagLayout gbl = new GridBagLayout();
        gbl.columnWeights = new double[] { 0.0, 1.0 };
        setLayout(gbl);

        // Mode
        modeModel.setSelectedItem(margs.mode);
        wMode.addActionListener(e -> FileAccessBenchPanel.this.margs.mode = modeModel.getSelectedItem());
        add(0, "File access mode", wMode);

        // Method
        methodModel.setSelectedItem(margs.method);
        wMethod.addActionListener(e -> FileAccessBenchPanel.this.margs.method = methodModel.getSelectedItem());

        add(1, "File access method", wMethod);

        // Chunk size
        wChunkSize.setColumns(10);
        wChunkSize.setHorizontalAlignment(SwingConstants.TRAILING);
        wChunkSize.setText("" + margs.chunksSize);
        wChunkSize.getDocument().addDocumentListener(documentListener);
        add(2, "Chunks size (Bytes)", wChunkSize);

        // Chunks count
        wChunksCount.setColumns(10);
        wChunksCount.setHorizontalAlignment(SwingConstants.TRAILING);
        wChunksCount.setText("" + margs.chunksCount);
        wChunksCount.getDocument().addDocumentListener(documentListener);
        add(3, "Number of chunks per file", wChunksCount);

        // Delay
        wDelay.setColumns(10);
        wDelay.setHorizontalAlignment(SwingConstants.TRAILING);
        wDelay.setText("" + margs.delayBetweenTests);
        wDelay.getDocument().addDocumentListener(documentListener);
        add(4, "Delay between tests (seconds)", wDelay);

        // Samples
        wSamples.setColumns(10);
        wSamples.setHorizontalAlignment(SwingConstants.TRAILING);
        wSamples.setText("" + margs.numberOfSamplesPerTest);
        wSamples.getDocument().addDocumentListener(documentListener);
        add(5, "Number of samples per test", wSamples);

        // Tests
        wTests.setColumns(10);
        wTests.setHorizontalAlignment(SwingConstants.TRAILING);
        wTests.setText("" + margs.numberOfTests);
        wTests.getDocument().addDocumentListener(documentListener);
        add(6, "Number of tests", wTests);

        // Execute
        add(7, null, wStartStop);
        wStartStop.addActionListener(e -> setRunning(!running));

    }

    protected void setRunning(boolean value) {
        running = value;
        wMode.setEnabled(!running);
        wMethod.setEnabled(!running);
        wChunkSize.setEnabled(!running);
        wChunksCount.setEnabled(!running);
        wDelay.setEnabled(!running);
        wSamples.setEnabled(!running);
        wTests.setEnabled(!running);
        if (running) {
            wStartStop.setText("Stop");
            execute();
        } else {
            worker.cancel(false);
            wStartStop.setText("Start");
        }
    }

    private void execute() {
        worker = new MonitorWorker(margs);
        worker.execute();
    }

    protected void refresh() {
        if (wChunkSize.getStatus() == Status.ERROR
                || wChunksCount.getStatus() == Status.ERROR
                || wDelay.getStatus() == Status.ERROR
                || wSamples.getStatus() == Status.ERROR
                || wTests.getStatus() == Status.ERROR) {
            wStartStop.setEnabled(false);
        } else {
            wStartStop.setEnabled(true);
            margs.chunksCount = Integer.parseInt(wChunksCount.getText());
            margs.chunksSize = Integer.parseInt(wChunkSize.getText());
            margs.numberOfTests = Integer.parseInt(wTests.getText());
            margs.numberOfSamplesPerTest = Integer.parseInt(wSamples.getText());
            margs.delayBetweenTests = Double.parseDouble(wDelay.getText());
        }
    }

    private class MonitorWorker extends SwingBenchExecutor {
        public MonitorWorker(FileAccessBench.MainArgs margs) {
            super(new FileAccessBench(margs), margs);
        }
    }
}