package cdc.bench.io.files.ui;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.bench.io.files.FileAccessBench;
import cdc.bench.io.files.FileAccessBenchSupport;
import cdc.bench.support.Config;
import cdc.ui.swing.SwingUtils;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.MainResult;

public final class FileAccessBenchUi {
    private static final Logger LOGGER = LogManager.getLogger(FileAccessBenchUi.class);

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        // Do not exit !
        exec(args).getCode();
    }

    private static class MainSupport extends AbstractMainSupport<FileAccessBench.MainArgs, Void> {
        public MainSupport() {
            super(FileAccessBench.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            FileAccessBenchSupport.addSpecificOptions(options);
        }

        @Override
        protected FileAccessBench.MainArgs analyze(CommandLine cl) throws ParseException {
            final FileAccessBench.MainArgs margs = new FileAccessBench.MainArgs();
            FileAccessBenchSupport.analyze(cl, margs);
            return margs;
        }

        @Override
        protected Void execute(FileAccessBench.MainArgs margs) throws Exception {
            SwingUtilities.invokeLater(() -> {
                final JFrame wFrame = new JFrame();
                wFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                wFrame.setTitle(FileAccessBenchUi.class.getSimpleName());
                wFrame.setIconImage(SwingUtils.getApplicationImage());
                wFrame.getContentPane().add(new FileAccessBenchPanel(margs));
                wFrame.pack();
                wFrame.setVisible(true);
            });
            return null;
        }
    }
}