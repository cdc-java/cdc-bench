package cdc.bench.io.files;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import cdc.util.lang.ExceptionWrapper;

public class RandomAccessFileTestCase extends TestCase {

    public RandomAccessFileTestCase() {
        super("RandomAccessFile");
    }

    @Override
    public double testRead(File file,
                           Settings settings,
                           Analyzer analyzer) {
        analyzer.processBegin();
        try (final RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            final byte[] buffer = new byte[settings.getChunkSize()];
            int bytesRead;
            while (-1 != (bytesRead = raf.read(buffer))) {
                for (int i = 0; i < bytesRead; i++) {
                    final byte b = buffer[i];
                    analyzer.processData(b);
                }
            }
            analyzer.processEnd();
            return settings.getFileSize();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public double testWrite(File file,
                            Settings settings,
                            Analyzer analyzer) {
        analyzer.processBegin();
        try (final RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            final byte[] chunk = settings.getChunk();
            final byte[] buffer = new byte[settings.getChunkSize()];
            for (int chunkIndex = 0; chunkIndex < settings.getChunkCount(); chunkIndex++) {
                int pos = 0;
                for (int index = 0; index < chunk.length; index++) {
                    final byte b = chunk[index];
                    analyzer.processData(b);
                    buffer[pos++] = b;
                }
                raf.write(buffer, 0, settings.getChunkSize());
            }
            analyzer.processEnd();
            return settings.getFileSize();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }
}