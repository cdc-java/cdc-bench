package cdc.bench.io.files;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.bench.support.Formatting;

public class Test00 {
    private static final Logger LOGGER = LogManager.getLogger(Test00.class);

    public static void main(String[] args) {

        final TestCase[] testcases = new TestCase[] {
                new BufferedStreamFileTestCase(),
                new BufferedChannelFileTestCase(),
                new RandomAccessFileTestCase(),
                new MemoryMappedFileTestCase()
        };
        int chunkSize = 1;
        int chunkCount = 5 * 1024 * 1024;

        for (int i = 0; i < 16; i++) {
            final Settings settings = new Settings(chunkSize, chunkCount);
            final ChecksumAnalyzer analyzer = new ChecksumAnalyzer();

            LOGGER.info("{}", settings);
            for (final TestCase testCase : testcases) {
                testCase.execute(new File("out.txt"), settings, analyzer, AccessMode.WRITE);
                final long dtWriteNano = analyzer.getChronometer().getElapsedNanos();
                final int checksumWrite = analyzer.getChecksum();
                final long countWrite = analyzer.getCount();
                testCase.execute(new File("out.txt"), settings, analyzer, AccessMode.READ);
                final long dtReadNano = analyzer.getChronometer().getElapsedNanos();
                final int checksumRead = analyzer.getChecksum();
                final long countRead = analyzer.getCount();

                if (settings.getFileSize() != countWrite || settings.getFileSize() != countRead) {
                    LOGGER.error("Count expected: {}", settings.getFileSize());
                    LOGGER.error("Count write   : {}", countWrite);
                    LOGGER.error("Count read    : {}", countRead);
                }

                if (checksumRead != checksumWrite) {
                    LOGGER.error("Checksum mismatch");
                    LOGGER.error("Checksum write: {}", checksumWrite);
                    LOGGER.error("Checksum read : {}", checksumRead);
                }
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info("   {}\n write: {}  read: {}",
                                testCase.getName(),
                                Formatting.formatRate(countWrite, dtWriteNano),
                                Formatting.formatRate(countRead, dtReadNano));
                }
            }
            chunkSize *= 2;
            chunkCount /= 2;
        }
    }
}