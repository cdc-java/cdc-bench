package cdc.bench.io.files;

import cdc.bench.support.Formatting;

public class Settings {
    private final int chunkSize;
    private final int chunkCount;
    private final long fileSize;
    private final byte[] chunk;

    public Settings(int chunkSize,
                    int chunkCount) {
        this.chunkSize = chunkSize;
        this.chunkCount = chunkCount;
        this.fileSize = chunkSize * (long) chunkCount;
        this.chunk = new byte[chunkSize];
        for (int index = 0; index < chunk.length - 1; index++) {
            final int mod = index % 27;
            if (mod == 26) {
                chunk[index] = '\n';
            } else {
                chunk[index] = (byte) (mod + 'A');
            }
            chunk[chunk.length - 1] = '\n';
        }
    }

    public Settings(String chunk,
                    int chunkCount) {
        this.chunk = chunk.getBytes();
        this.chunkSize = this.chunk.length;
        this.chunkCount = chunkCount;
        this.fileSize = chunkSize * (long) chunkCount;
    }

    public final int getChunkSize() {
        return chunkSize;
    }

    public final long getChunkCount() {
        return chunkCount;
    }

    public final long getFileSize() {
        return fileSize;
    }

    public final byte[] getChunk() {
        return chunk;
    }

    @Override
    public String toString() {
        return "chunk size: " + Formatting.formatSize(chunkSize)
                + " chunks: " + chunkCount
                + " file size: " + Formatting.formatSize(fileSize);
    }
}