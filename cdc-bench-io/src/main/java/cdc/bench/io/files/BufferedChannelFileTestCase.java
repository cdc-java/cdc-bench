package cdc.bench.io.files;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import cdc.util.lang.ExceptionWrapper;

public class BufferedChannelFileTestCase extends TestCase {
    public BufferedChannelFileTestCase() {
        super("BufferedChannelFile");
    }

    @Override
    public double testRead(File file,
                           Settings settings,
                           Analyzer analyzer) {
        analyzer.processBegin();
        try (final RandomAccessFile raf = new RandomAccessFile(file, "r");
                final FileChannel channel = raf.getChannel()) {
            final ByteBuffer buffer = ByteBuffer.allocate(settings.getChunkSize());

            while (-1 != (channel.read(buffer))) {
                buffer.flip();
                while (buffer.hasRemaining()) {
                    final byte b = buffer.get();
                    analyzer.processData(b);
                }
                buffer.clear();
            }
            analyzer.processEnd();
            return settings.getFileSize();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public double testWrite(File file,
                            Settings settings,
                            Analyzer analyzer) {
        analyzer.processBegin();
        try (final RandomAccessFile raf = new RandomAccessFile(file, "rw");
                final FileChannel channel = raf.getChannel()) {
            final ByteBuffer buffer = ByteBuffer.allocate(settings.getChunkSize());
            final byte[] chunk = settings.getChunk();
            for (int chunkIndex = 0; chunkIndex < settings.getChunkCount(); chunkIndex++) {
                for (int index = 0; index < chunk.length; index++) {
                    final byte b = chunk[index];
                    analyzer.processData(b);
                    buffer.put(b);
                    if (!buffer.hasRemaining()) {
                        buffer.flip();
                        channel.write(buffer);
                        buffer.clear();
                    }
                }
            }
            analyzer.processEnd();
            return settings.getFileSize();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }
}