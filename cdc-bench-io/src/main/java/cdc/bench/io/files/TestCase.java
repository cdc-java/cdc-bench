package cdc.bench.io.files;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class TestCase {
    private static final Logger LOGGER = LogManager.getLogger(TestCase.class);
    private final String name;

    protected TestCase(String name) {
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    public final double execute(File file,
                                Settings settings,
                                Analyzer analyzer,
                                AccessMode mode) {
        try {
            if (mode == AccessMode.READ) {
                return testRead(file, settings, analyzer);
            } else {
                return testWrite(file, settings, analyzer);
            }
        } catch (final RuntimeException e) {
            LOGGER.catching(e);
        }
        return 0.0;
    }

    public abstract double testRead(File file,
                                    Settings settings,
                                    Analyzer analyzer);

    public abstract double testWrite(File file,
                                     Settings settings,
                                     Analyzer analyzer);
}