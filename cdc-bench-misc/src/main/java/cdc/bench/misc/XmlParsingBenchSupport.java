package cdc.bench.misc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

import cdc.io.data.xml.XmlDataReader;
import cdc.io.xml.XmlWriter;

public class XmlParsingBenchSupport {
    private static class SaxHandler extends DefaultHandler2 {
        int elements = 0;

        SaxHandler() {
        }

        @Override
        public void startElement(String uri,
                                 String localName,
                                 String qName,
                                 Attributes attributes) throws SAXException {
            elements++;
        }
    }

    private byte[] b;

    private ByteArrayInputStream newByteArrayInputStream() {
        return new ByteArrayInputStream(b);
    }

    public void prepare(int children,
                        int attributes) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream(100_000_000);
        try (final XmlWriter writer = new XmlWriter(out)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            writer.beginDocument();
            writer.beginElement("root");
            for (int i = 0; i < children; i++) {
                writer.beginElement("child");
                for (int j = 0; j < attributes; j++) {
                    writer.addAttribute("att" + j, "value");
                }
                writer.endElement();
            }
            writer.endElement();
            writer.endDocument();
        }
        b = out.toByteArray();
    }

    // SAX

    public static int testSaxParser(SAXParserFactory factory,
                                    InputStream is) throws ParserConfigurationException, SAXException, IOException {
        final SaxHandler handler = new SaxHandler();
        final SAXParser parser = factory.newSAXParser();
        final InputSource source = new InputSource(is);
        parser.parse(source, handler);
        return handler.elements;
    }

    public int testSaxParser(SAXParserFactory factory) throws ParserConfigurationException, SAXException, IOException {
        return testSaxParser(factory, newByteArrayInputStream());
    }

    // DOM

    public static org.w3c.dom.Document testDomReader(DocumentBuilderFactory factory,
                                                     InputStream is) throws ParserConfigurationException, SAXException, IOException {
        final DocumentBuilder builder = factory.newDocumentBuilder();
        final InputSource source = new InputSource(is);
        return builder.parse(source);
    }

    public org.w3c.dom.Document
            testDomReader(DocumentBuilderFactory factory) throws ParserConfigurationException, SAXException, IOException {
        return testDomReader(factory, newByteArrayInputStream());
    }

    // StAX Stream

    public static int testStAXStreamReader(XMLInputFactory factory,
                                           InputStream is) throws XMLStreamException {
        final XMLStreamReader reader = factory.createXMLStreamReader(is);
        int elements = 0;
        do {
            final int eventType = reader.getEventType();
            if (eventType == XMLStreamConstants.START_ELEMENT) {
                elements++;
            }
            reader.next();
        } while (reader.hasNext());
        return elements;
    }

    // StAX Event

    public int testStAXStreamReader(XMLInputFactory factory) throws XMLStreamException {
        return testStAXStreamReader(factory, newByteArrayInputStream());
    }

    public static int testStAXEventReader(XMLInputFactory factory,
                                          InputStream is) throws XMLStreamException {
        final XMLEventReader reader = factory.createXMLEventReader(is);
        int elements = 0;
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if (event.getEventType() == XMLStreamConstants.START_ELEMENT) {
                elements++;
            }
        }
        return elements;
    }

    public int testStAXEventReader(XMLInputFactory factory) throws XMLStreamException {
        return testStAXEventReader(factory, newByteArrayInputStream());
    }

    // CDC Data

    public cdc.io.data.Document testDataSAXReader() throws IOException {
        final XmlDataReader reader = new XmlDataReader();
        final ByteArrayInputStream is = new ByteArrayInputStream(b);
        return reader.read(is);
    }

    public cdc.io.data.Document testDataStAXReader() throws IOException {
        final XmlDataReader reader = new XmlDataReader();
        reader.setEnabled(XmlDataReader.Feature.USE_STAX, true);
        final ByteArrayInputStream is = new ByteArrayInputStream(b);
        return reader.read(is);
    }

    // JDOM

    public org.jdom2.Document testJDomSAXBuilder() throws JDOMException, IOException {
        final SAXBuilder builder = new SAXBuilder();
        builder.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        builder.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        return builder.build(newByteArrayInputStream());
    }

    // SAX

    public int testSaxParserJDK() throws ParserConfigurationException, SAXException, IOException {
        return testSaxParser(SAXParserFactory.newInstance());
    }

    public int testSaxParserAalto() throws ParserConfigurationException, SAXException, IOException {
        return testSaxParser(new com.fasterxml.aalto.sax.SAXParserFactoryImpl());
    }

    public int testSaxParserWoodstox() throws ParserConfigurationException, SAXException, IOException {
        return testSaxParser(new com.ctc.wstx.sax.WstxSAXParserFactory());
    }

    // DOM

    public org.w3c.dom.Document testDomReaderJDK() throws ParserConfigurationException, SAXException, IOException {
        return testDomReader(DocumentBuilderFactory.newInstance());
    }

    // StAX Stream

    public int testStAXStreamReaderJDK() throws XMLStreamException {
        return testStAXStreamReader(XMLInputFactory.newInstance());
    }

    public int testStAXStreamReaderAalto() throws XMLStreamException {
        return testStAXStreamReader(new com.fasterxml.aalto.stax.InputFactoryImpl());
    }

    public int testStAXStreamReaderWoodstox() throws XMLStreamException {
        return testStAXStreamReader(new com.ctc.wstx.stax.WstxInputFactory());
    }

    // StAX Event

    public int testStAXEventReaderJDK() throws XMLStreamException {
        return testStAXEventReader(XMLInputFactory.newInstance());
    }

    public int testStAXEventReaderAalto() throws XMLStreamException {
        return testStAXEventReader(new com.fasterxml.aalto.stax.InputFactoryImpl());
    }

    public int testStAXEventReaderWoodstox() throws XMLStreamException {
        return testStAXEventReader(new com.ctc.wstx.stax.WstxInputFactory());
    }
}