package cdc.bench.misc;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms2G", "-Xmx2G" })
@Warmup(iterations = 1, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 1, time = 1, timeUnit = TimeUnit.SECONDS)
public class ChainBench {

    private static class X {
        int x = 0;

        public X() {
            super();
        }

        public X addChained(int x) {
            this.x += x * x;
            return this;
        }

        public void addUnchained(int x) {
            this.x += x * x;
        }
    }

    @Benchmark
    public int testUnchained() {
        final X x = new X();
        x.addUnchained(1);
        x.addUnchained(2);
        x.addUnchained(4);
        x.addUnchained(8);
        x.addUnchained(16);
        x.addUnchained(32);
        x.addUnchained(64);
        return x.x;
    }

    @Benchmark
    public int testChainedNo() {
        final X x = new X();
        x.addChained(1);
        x.addChained(2);
        x.addChained(4);
        x.addChained(8);
        x.addChained(16);
        x.addChained(32);
        x.addChained(64);
        return x.x;
    }

    @Benchmark
    public int testChainedYes() {
        return new X().addChained(1)
                      .addChained(2)
                      .addChained(4)
                      .addChained(8)
                      .addChained(16)
                      .addChained(32)
                      .addChained(64).x;
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(ChainBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", ChainBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}