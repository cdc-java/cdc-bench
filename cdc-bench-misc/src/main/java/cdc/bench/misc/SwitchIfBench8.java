package cdc.bench.misc;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.util.bench.BenchUtils;
import cdc.util.lang.UnexpectedValueException;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 500, timeUnit = TimeUnit.MILLISECONDS)
public class SwitchIfBench8 {
    public enum Kind {
        C1,
        C2,
        C3,
        C4,
        C5,
        C6,
        C7,
        C8,
    }

    @Param({ "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8" })
    private Kind kind;

    @Benchmark
    public String benchSwitch() {
        switch (kind) {
        case C1:
            return "C1";
        case C2:
            return "C2";
        case C3:
            return "C3";
        case C4:
            return "C4";
        case C5:
            return "C4";
        case C6:
            return "C6";
        case C7:
            return "C7";
        case C8:
            return "C8";
        default:
            throw new UnexpectedValueException(kind);
        }
    }

    @Benchmark
    public String benchIf() {
        if (kind == Kind.C1) {
            return "C1";
        } else if (kind == Kind.C2) {
            return "C2";
        } else if (kind == Kind.C3) {
            return "C3";
        } else if (kind == Kind.C4) {
            return "C4";
        } else if (kind == Kind.C5) {
            return "C5";
        } else if (kind == Kind.C6) {
            return "C6";
        } else if (kind == Kind.C7) {
            return "C7";
        } else if (kind == Kind.C8) {
            return "C8";
        } else {
            throw new UnexpectedValueException(kind);
        }
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(SwitchIfBench8.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", SwitchIfBench8.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}