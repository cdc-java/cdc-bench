package cdc.bench.misc;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.jdom2.JDOMException;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.xml.sax.SAXException;

import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms2G", "-Xmx8G" })
@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 100, time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class XmlParsingBench extends XmlParsingBenchSupport {

    @Param({ "1", "10", "100", "1000", "10000", "100000"/* , "1000000", "10000000" */ })
    public int size;

    @Setup
    public void prepare() throws IOException {
        super.prepare(size, 0);
    }

    // CDC Data

    @Benchmark
    @Override
    public cdc.io.data.Document testDataSAXReader() throws IOException {
        return super.testDataSAXReader();
    }

    @Benchmark
    @Override
    public cdc.io.data.Document testDataStAXReader() throws IOException {
        return super.testDataStAXReader();
    }

    // JDOM

    @Benchmark
    @Override
    public org.jdom2.Document testJDomSAXBuilder() throws JDOMException, IOException {
        return super.testJDomSAXBuilder();
    }

    // SAX

    @Benchmark
    @Override
    public int testSaxParserJDK() throws ParserConfigurationException, SAXException, IOException {
        return super.testSaxParserJDK();
    }

    @Benchmark
    @Override
    public int testSaxParserAalto() throws ParserConfigurationException, SAXException, IOException {
        return super.testSaxParserAalto();
    }

    @Benchmark
    @Override
    public int testSaxParserWoodstox() throws ParserConfigurationException, SAXException, IOException {
        return super.testSaxParserWoodstox();
    }

    // DOM

    @Benchmark
    @Override
    public org.w3c.dom.Document testDomReaderJDK() throws ParserConfigurationException, SAXException, IOException {
        return super.testDomReaderJDK();
    }

    // StAX Stream

    @Benchmark
    @Override
    public int testStAXStreamReaderJDK() throws XMLStreamException {
        return super.testStAXStreamReaderJDK();
    }

    @Benchmark
    @Override
    public int testStAXStreamReaderAalto() throws XMLStreamException {
        return super.testStAXStreamReaderAalto();
    }

    @Benchmark
    @Override
    public int testStAXStreamReaderWoodstox() throws XMLStreamException {
        return super.testStAXStreamReaderWoodstox();
    }

    // StAX Event

    @Benchmark
    @Override
    public int testStAXEventReaderJDK() throws XMLStreamException {
        return super.testStAXEventReaderJDK();
    }

    @Benchmark
    @Override
    public int testStAXEventReaderAalto() throws XMLStreamException {
        return super.testStAXEventReaderAalto();
    }

    @Benchmark
    @Override
    public int testStAXEventReaderWoodstox() throws XMLStreamException {
        return super.testStAXEventReaderWoodstox();
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(XmlParsingBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", XmlParsingBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}