package cdc.bench.misc;

import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms2G", "-Xmx8G" })
@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class CharSetBench {
    protected static final Logger LOGGER = LogManager.getLogger(CharSetBench.class);

    @Param({ " (){},~=&|!", " (){},~=&|!∧∨∈¬∉≠→↔⊤⊥" })
    public String chars;

    @Param({ "a", "!", " ", "~", "|", "∧", "⊥" })
    public char searched;

    private BooleanSet bset;
    private StandardHashSet shset;
    private StandardBitSet sbset;
    private BinarySearchSet bsset;
    private LinearSearchSet lsset;

    @Setup
    public void prepare() {
        bset = new BooleanSet(chars);
        shset = new StandardHashSet(chars);
        sbset = new StandardBitSet(chars);
        bsset = new BinarySearchSet(chars);
        lsset = new LinearSearchSet(chars);
    }

    public static interface CharSet {
        public boolean contains(char c);
    }

    protected static int max(String chars) {
        int max = -1;
        for (int index = 0; index < chars.length(); index++) {
            max = Math.max(max, chars.charAt(index));
        }
        return max;
    }

    public static class BooleanSet implements CharSet {
        private final boolean[] mask;
        private final int maskLength;

        public BooleanSet(String chars) {
            mask = build(chars);
            maskLength = mask.length;
            LOGGER.info("BooleanSet({}): {}", chars, maskLength);
        }

        private static boolean[] build(String chars) {
            final int max = max(chars);
            final boolean[] tmp = new boolean[max + 1];
            for (int index = 0; index < tmp.length; index++) {
                tmp[index] = false;
            }
            for (int index = 0; index < chars.length(); index++) {
                tmp[chars.charAt(index)] = true;
            }
            return tmp;
        }

        @Override
        public boolean contains(char c) {
            return c < maskLength && mask[c];
        }
    }

    public static class BinarySearchSet implements CharSet {
        private final char[] chars;

        public BinarySearchSet(String chars) {
            this.chars = new char[chars.length()];
            chars.getChars(0, chars.length(), this.chars, 0);
            Arrays.sort(this.chars);
        }

        @Override
        public boolean contains(char c) {
            return Arrays.binarySearch(chars, c) >= 0;
        }
    }

    public static class LinearSearchSet implements CharSet {
        private final char[] chars;
        private final int length;

        public LinearSearchSet(String chars) {
            this.chars = new char[chars.length()];
            chars.getChars(0, chars.length(), this.chars, 0);
            length = chars.length();
        }

        @Override
        public boolean contains(char c) {
            for (int index = 0; index < length; index++) {
                if (chars[index] == c) {
                    return true;
                }
            }
            return false;
        }
    }

    public static class StandardHashSet implements CharSet {
        private final Set<Character> set = new HashSet<>();

        public StandardHashSet(String chars) {
            for (int index = 0; index < chars.length(); index++) {
                set.add(chars.charAt(index));
            }
        }

        @Override
        public boolean contains(char c) {
            return set.contains(c);
        }
    }

    public static class StandardBitSet implements CharSet {
        private final BitSet set;

        public StandardBitSet(String chars) {
            set = new BitSet(max(chars));
            for (int index = 0; index < chars.length(); index++) {
                set.set(chars.charAt(index));
            }
            LOGGER.info("StandardBitSet({}): {}", chars, set.size());
        }

        @Override
        public boolean contains(char c) {
            return set.get(c);
        }
    }

    @Benchmark
    public boolean testBooleanSet() {
        return bset.contains(searched);
    }

    @Benchmark
    public boolean testStandardHashSet() {
        return shset.contains(searched);
    }

    @Benchmark
    public boolean testStandardBitSet() {
        return sbset.contains(searched);
    }

    @Benchmark
    public boolean testBinarySearchSet() {
        return bsset.contains(searched);
    }

    @Benchmark
    public boolean testLinearSearchSet() {
        return lsset.contains(searched);
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(CharSetBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", CharSetBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}