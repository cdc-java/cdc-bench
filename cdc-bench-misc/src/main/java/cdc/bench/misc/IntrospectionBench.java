package cdc.bench.misc;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.util.bench.BenchUtils;
import cdc.util.lang.Introspection;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class IntrospectionBench {
    public static class A {
        public A() {
            super();
        }

        public static class AA {
            public AA() {
                super();
            }

            public static class AAA {
                public AAA() {
                    super();
                }
            }
        }
    }

    @Param({
            "java.lang.String",
            "java.lang.Integer",
            "java.lang.Double",
            "java.util.List",
            "java.util.Set",
            "cdc.util.bench.BenchUtils",
            "cdc.util.Config",
            "cdc.util.debug.Debug",
            "cdc.bench.misc.IntrospectionBench$A",
            "cdc.bench.misc.IntrospectionBench$A$AA",
            "cdc.bench.misc.IntrospectionBench$A$AA$AAA",
            "cdc.bench.misc.IntrospectionBench",
            "cdc.bench.misc.InstanceOfBench" })
    private String className;

    @Benchmark
    public Class<?> benchJavaClassForName() throws ClassNotFoundException {
        return Class.forName(className, false, ClassLoader.getSystemClassLoader());
    }

    @Benchmark
    public Class<?> benchIntrospectionGetClass() {
        return Introspection.getClass(className);
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(IntrospectionBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", IntrospectionBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}