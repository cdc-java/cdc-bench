package cdc.bench.misc;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class Generator {
    private final String pack;
    private final String base;
    final File path;
    final File fpath;

    public Generator(String pack,
                     String base) {
        this.pack = pack;
        this.base = base;
        this.path = new File(pack.replaceAll("\\.", "/"));
        this.fpath = new File("src/main/java", path.getPath());
        this.fpath.mkdirs();
    }

    private String toName(int index) {
        return String.format("%s%05d", base, index);
    }

    public void generate(int count) throws IOException {
        for (int index = 0; index < count; index++) {
            final String name = toName(index);
            final File file = new File(fpath, name + ".java");
            System.out.println(file);

            try (final PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream(file)), false, "UTF-8")) {
                final String prevName = toName(index - 1);
                out.println("package " + pack + ";");
                out.println();
                out.println("import cdc.bench.misc.Traces;");
                out.println();

                out.println("public class " + name + " {");
                if (index > 0) {
                    out.println("    private final " + prevName + " prev;");
                    out.println();
                }
                out.println("    public " + name + "() {");
                out.println("        super();");
                out.println("        Traces.trace(\"" + name + ".<init>(\" + this + \")\");");
                if (index > 0) {
                    out.println("        this.prev = new " + prevName + "();");
                }
                out.println("    }");
                out.println();

                // out.println(" static {");
                // out.println(" Traces.trace(\"" + name + ".<sinit>()\");");
                // out.println(" }");
                // out.println();

                if (index > 0) {
                    out.println("    public " + prevName + " prev() {");
                    out.println("        return prev;");
                    out.println("    }");
                    out.println();
                }
                out.println("    public void print() {");
                out.println("        Traces.trace(this);");
                if (index > 0) {
                    out.println("        prev().print();");
                }
                out.println("    }");
                out.println();
                out.println("    @Override");
                out.println("    public String toString() {");
                out.println("        return super.toString();");
                out.println("    }");
                out.print("}");
            }
        }
        generateMain1(count);
        generateMain2(count);
        generateMain3(count);
    }

    private void generateMain1(int count) throws IOException {
        final String name = base + "Main1";
        final File file = new File(fpath, name + ".java");
        try (final PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream(file)), false, "UTF-8")) {
            out.println("package " + pack + ";");
            out.println();
            out.println("import cdc.bench.misc.Traces;");
            out.println();
            out.println("public class " + name + " {");
            out.println();
            out.println("    public static void main(String[] args) {");
            out.println("        Traces.trace(\"" + name + "\");");
            out.println("        final " + toName(count - 1) + " x = new " + toName(count - 1) + "();");
            out.println("        x.print();");
            out.println("    }");
            out.print("}");
        }
    }

    private void generateMain2(int count) throws IOException {
        final String name = base + "Main2";
        final File file = new File(fpath, name + ".java");
        try (final PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream(file)), false, "UTF-8")) {
            out.println("package " + pack + ";");
            out.println();
            out.println("import cdc.bench.misc.Traces;");
            out.println("import java.util.List;");
            out.println("import java.util.ArrayList;");
            out.println("import cdc.util.refs.ClassRef;");
            out.println("import cdc.util.refs.EagerClassRef;");
            out.println();
            out.println("public class " + name + " {");
            out.println();
            out.println("    public static void main(String[] args) {");
            out.println("        Traces.trace(\"START " + name + "\");");
            out.println("        List<ClassRef> refs = new ArrayList<>();");
            for (int index = 0; index < count; index++) {
                out.println("        refs.add(new EagerClassRef(" + toName(index) + ".class));");
            }
            out.println();
            out.println("        Traces.trace(\"RESOLVE " + name + "\");");
            out.println("        for (ClassRef ref : refs) {");
            out.println("           ref.resolve();");
            out.println("        }");
            out.println("        Traces.trace(\"END " + name + "\");");
            out.println("    }");
            out.print("}");
        }
    }

    private void generateMain3(int count) throws IOException {
        final String name = base + "Main3";
        final File file = new File(fpath, name + ".java");
        try (final PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream(file)), false, "UTF-8")) {
            out.println("package " + pack + ";");
            out.println();
            out.println("import cdc.bench.misc.Traces;");
            out.println("import java.util.List;");
            out.println("import java.util.ArrayList;");
            out.println("import cdc.util.refs.ClassRef;");
            out.println("import cdc.util.refs.LazyClassRef;");
            out.println();
            out.println("public class " + name + " {");
            out.println();
            out.println("    public static void main(String[] args) {");
            out.println("        Traces.trace(\"START " + name + "\");");
            out.println("        List<ClassRef> refs = new ArrayList<>();");
            for (int index = 0; index < count; index++) {
                out.println("        refs.add(new LazyClassRef(\"" + pack + "." + toName(index) + "\"));");
            }
            out.println();
            out.println("        Traces.trace(\"RESOLVE " + name + "\");");
            out.println("        for (ClassRef ref : refs) {");
            out.println("           ref.resolve();");
            out.println("        }");
            out.println("        Traces.trace(\"END " + name + "\");");
            out.println("    }");
            out.print("}");
        }
    }

    public static void main(String[] args) throws IOException {
        final Generator generator = new Generator("cdc.bench.gen", "Bench");
        generator.generate(1000);
    }
}