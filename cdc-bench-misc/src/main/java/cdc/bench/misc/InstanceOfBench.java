package cdc.bench.misc;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 500, timeUnit = TimeUnit.MILLISECONDS)
public class InstanceOfBench {

    private enum Kind {
        C1,
        C2,
        C3
    }

    public static final C[] OBJECTS = {
            new C1(),
            new C2(),
            new C3(),
            new C1(),
            new C2(),
            new C3(),
            new C1(),
            new C2(),
            new C3()
    };

    private static abstract class C {
        public C() {
            super();
        }

        public abstract Kind getKind();
    }

    private static class C1 extends C {
        public C1() {
            super();
        }

        @Override
        public Kind getKind() {
            return Kind.C1;
        }
    }

    private static class C2 extends C {
        public C2() {
            super();
        }

        @Override
        public Kind getKind() {
            return Kind.C2;
        }
    }

    private static class C3 extends C {
        public C3() {
            super();
        }

        @Override
        public Kind getKind() {
            return Kind.C3;
        }
    }

    private static boolean isInstanceOfC1(C c) {
        return c instanceof C1;
    }

    private static boolean isInstanceOfC2(C c) {
        return c instanceof C2;
    }

    private static boolean isInstanceOfC3(C c) {
        return c instanceof C3;
    }

    @Benchmark
    public int benchInstanceOfC1() {
        int count = 0;
        for (final C c : OBJECTS) {
            if (c instanceof C1) {
                count++;
            }
        }
        return count;
    }

    @Benchmark
    public int benchInstanceOfC2() {
        int count = 0;
        for (final C c : OBJECTS) {
            if (c instanceof C2) {
                count++;
            }
        }
        return count;
    }

    @Benchmark
    public int benchInstanceOfC3() {
        int count = 0;
        for (final C c : OBJECTS) {
            if (c instanceof C3) {
                count++;
            }
        }
        return count;
    }

    @Benchmark
    public int benchInstanceOfC1Bis() {
        int count = 0;
        for (final C c : OBJECTS) {
            if (isInstanceOfC1(c)) {
                count++;
            }
        }
        return count;
    }

    @Benchmark
    public int benchInstanceOfC2Bis() {
        int count = 0;
        for (final C c : OBJECTS) {
            if (isInstanceOfC2(c)) {
                count++;
            }
        }
        return count;
    }

    @Benchmark
    public int benchInstanceOfC3Bis() {
        int count = 0;
        for (final C c : OBJECTS) {
            if (isInstanceOfC3(c)) {
                count++;
            }
        }
        return count;
    }

    @Benchmark
    public int benchKindOfC1() {
        int count = 0;
        for (final C c : OBJECTS) {
            if (c.getKind() == Kind.C1) {
                count++;
            }
        }
        return count;
    }

    @Benchmark
    public int benchKindOfC2() {
        int count = 0;
        for (final C c : OBJECTS) {
            if (c.getKind() == Kind.C2) {
                count++;
            }
        }
        return count;
    }

    @Benchmark
    public int benchKindOfC3() {
        int count = 0;
        for (final C c : OBJECTS) {
            if (c.getKind() == Kind.C3) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(InstanceOfBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", InstanceOfBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}