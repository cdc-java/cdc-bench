package cdc.bench.misc;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.perfs.api.RuntimeManager;
import cdc.perfs.api.RuntimeProbe;
import cdc.perfs.api.Source;
import cdc.util.time.Chronometer;

public final class XmlParsingBenchMain {
    private static final Logger LOGGER = LogManager.getLogger(XmlParsingBenchMain.class);
    private static final Source SOURCE = RuntimeManager.getSource(XmlParsingBenchMain.class);

    private XmlParsingBenchMain() {
    }

    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 10000; i++) {
            final RuntimeProbe probe = RuntimeManager.createProbe(SOURCE);
            probe.start("loop");
            probe.stop();
        }
        final Chronometer chrono = new Chronometer();
        LOGGER.info("start");
        final XmlParsingBenchSupport bench = new XmlParsingBenchSupport();
        bench.prepare(100_000, 0);
        LOGGER.info("loops");
        for (int i = 0; i < 1; i++) {
            LOGGER.info("===================");
            LOGGER.info("Start loop {}", i);
            chrono.start();
            bench.testDataSAXReader();
            chrono.suspend();
            LOGGER.info("   loop {}: {}", i, chrono);
            LOGGER.info("Stats:");
        }
        LOGGER.info("done");
    }
}