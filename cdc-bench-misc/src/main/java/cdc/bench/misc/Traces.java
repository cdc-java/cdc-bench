package cdc.bench.misc;

import java.util.Locale;

import cdc.util.time.RefTime;
import cdc.util.time.TimeUnit;

public final class Traces {
    private Traces() {
    }

    public static void trace(String message) {
        System.out.println(String.format(Locale.ENGLISH,
                                         "[BENCH] %09.6f %s",
                                         RefTime.nanosToDouble(RefTime.getUptimeNanos(), TimeUnit.SECOND),
                                         message));
    }

    public static void trace(Object object) {
        trace(object == null ? "null" : object.toString());
    }
}