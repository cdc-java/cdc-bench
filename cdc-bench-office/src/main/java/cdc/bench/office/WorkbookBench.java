package cdc.bench.office;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.TableSection;
import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.SingleShotTime)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5, time = 10, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 500, timeUnit = TimeUnit.MILLISECONDS)
public class WorkbookBench {

    @Param({ "10", "100", "1000", "10000" })
    private int rows;

    @Param({ "10", "20" })
    private int cols;

    @Param({ "csv", "xlsx" })
    private String ext;

    private void bench(WorkbookWriterFeatures features,
                       WorkbookWriterFactory.Hint... hints) throws IOException {
        final List<String> cells = new ArrayList<>();
        for (int col = 0; col < cols; col++) {
            cells.add("Text " + col);
        }

        final StringBuilder name = new StringBuilder();
        name.append("bench-").append(rows).append('x').append(cols);
        for (final WorkbookWriterFactory.Hint hint : hints) {
            name.append("-").append(hint);
        }
        name.append('.').append(ext);

        final File file = new File("target", name.toString());
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        for (final WorkbookWriterFactory.Hint hint : hints) {
            factory.setEnabled(hint, true);
        }
        try (final WorkbookWriter<?> writer = factory.create(file, features)) {
            writer.beginSheet("Sheet");
            for (int row = 0; row < rows; row++) {
                writer.addRow(TableSection.DATA, cells);
            }
            writer.flush();
        }
    }

    @Benchmark
    public void benchFast() throws IOException {
        bench(WorkbookWriterFeatures.STANDARD_FAST);
    }

    @Benchmark
    public void benchFastPoiStreaming() throws IOException {
        bench(WorkbookWriterFeatures.STANDARD_FAST,
              WorkbookWriterFactory.Hint.POI_STREAMING);
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(WorkbookBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", WorkbookBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}