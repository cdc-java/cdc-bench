package cdc.bench.net;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.bench.support.BasicBenchExecutor;
import cdc.bench.support.Bench;
import cdc.bench.support.BenchData;
import cdc.bench.support.ChunkSupport;
import cdc.bench.support.Formatting;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.lang.SystemUtils;

public class Client implements Bench {
    protected static final Logger LOGGER = LogManager.getLogger(Client.class);

    public static class MainArgs extends ChunkSupport.MainArgs {
        public int port;
        public String host;
        public double retryDelay;
    }

    private final MainArgs margs;
    private Socket socket = null;
    private BufferedReader reader = null;
    private PrintWriter writer = null;
    final String message;

    private String getContext() {
        return Formatting.getCurrentTime() + " [" + margs.benchName + " " + margs.host + " " + margs.port + "]";
    }

    public Client(MainArgs margs) {
        this.margs = margs;
        this.message = createMessage(margs.chunksSize);
    }

    @Override
    public void startBench(BenchData data,
                           List<String> values) {
        do {
            try {
                socket = new Socket(margs.host, margs.port);
            } catch (final UnknownHostException e) {
                socket = null;
                System.out.println(getContext() + ": No server found ...");
                SystemUtils.sleepInSeconds(margs.retryDelay);
            } catch (final IOException e) {
                socket = null;
                System.out.println(getContext() + ": No server found ...");
                SystemUtils.sleepInSeconds(margs.retryDelay);
            }
        } while (socket == null);
        System.out.println(getContext() + ": Server found ...");

        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            writer.println(getContext());
        } catch (final IOException e) {
            e.printStackTrace();
        }
        values.add("Name");
        values.add("Chunk Size");
        values.add("Chunks Count");
    }

    @Override
    public String getQuantityUnitName() {
        return "KiB";
    }

    @Override
    public double getQuantityUnit() {
        return 1024.0;
    }

    @Override
    public void startTest(BenchData data) {
        // Ignore
    }

    @Override
    public void startSample(BenchData data) {
        // Ignore
    }

    @Override
    public double sample(BenchData data) {
        int size = 0;
        for (int i = 0; i < margs.chunksCount; i++) {
            writer.println(message);
            size += message.length();
            try {
                @SuppressWarnings("unused")
                final String response = reader.readLine();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
        return size;
    }

    @Override
    public void endSample(BenchData data) {
        // Ignore
    }

    @Override
    public void endTest(BenchData data,
                        List<String> values) {
        values.add(margs.benchName + " " + margs.host + " " + margs.port);
        values.add(margs.chunksSize + "");
        values.add(margs.chunksCount + "");
    }

    @Override
    public void endBench(BenchData data) {
        System.out.println(getContext() + " sends end message");
        writer.println("END");
        try {
            reader.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        writer.close();
        try {
            socket.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        System.out.println(getContext() + " closed");
    }

    private static String createMessage(int size) {
        final int s = size / 2;
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s; i++) {
            builder.append("x");
        }
        return builder.toString();
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static String PORT = "port";
        private static String HOST = "host";
        private static String RETRY_DELAY = "retry-delay";

        public MainSupport() {
            super(Client.class, LOGGER);
        }

        @Override
        protected void addSpecificOptions(Options options) {
            ChunkSupport.addSpecificOptions(options);

            options.addOption(Option.builder()
                                    .longOpt(PORT)
                                    .desc("Port number (default 8080).")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(HOST)
                                    .desc("Host (default localhost).")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(RETRY_DELAY)
                                    .desc("Retry delay in seconds (default 5.0).")
                                    .hasArg()
                                    .build());
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();

            ChunkSupport.analyze(cl, margs);

            margs.host = getValueAsString(cl, HOST, "localhost");
            margs.port = getValueAsInt(cl, PORT, 8080);
            margs.retryDelay = getValueAsDouble(cl, RETRY_DELAY, 5.0);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            final Client instance = new Client(margs);
            final BasicBenchExecutor executor = new BasicBenchExecutor(instance, margs);
            executor.run();
            return null;
        }
    }

    public static void main(String[] args) {
        final MainSupport support = new MainSupport();
        support.main(args);
    }
}