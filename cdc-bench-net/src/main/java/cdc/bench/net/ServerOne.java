package cdc.bench.net;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ServerOne {
    private static final Logger LOGGER = LogManager.getLogger(ServerOne.class);
    static final int port = 8080;

    public static void main(String[] args) throws IOException {
        LOGGER.info("Server(" + port + ")");
        try (final ServerSocket ssocket = new ServerSocket(port)) {
            LOGGER.info("Server waiting client connection");
            try (final Socket socket = ssocket.accept();
                    final BufferedReader reader =
                            new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    final PrintWriter writer =
                            new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true)) {

                while (true) {
                    LOGGER.info("Server reading client message");
                    final String str = reader.readLine();
                    if (str == null || str.equals("END")) {
                        break;
                    }
                    LOGGER.info("Server received: " + str.length() + " chars");
                    writer.println(str);
                }
                LOGGER.info("Server(" + port + ") closed");
            }
        }
    }
}