package cdc.bench.net;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Service extends Thread {
    private static final Logger LOGGER = LogManager.getLogger(Service.class);
    private static int next = 1;
    private final int id;
    private final Socket socket;
    private BufferedReader reader = null;
    private PrintWriter writer = null;
    private String client = "???";
    private boolean verbose = false;

    private String getContext() {
        return "Service(" + id + ") --> " + client;
    }

    public void setVerbose(boolean value) {
        this.verbose = value;
    }

    public Service(Socket socket) {
        this.id = next;
        next++;
        this.socket = socket;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            start();
        } catch (final IOException e) {
            LOGGER.catching(e);
            try {
                socket.close();
            } catch (final IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        try {
            client = reader.readLine();
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
        LOGGER.info(getContext() + " started");

        while (true) {
            String message = null;
            try {
                message = reader.readLine();
            } catch (final IOException e) {
                LOGGER.catching(e);
            }
            if (message == null || message.equals("END")) {
                break;
            }
            if (verbose) {
                LOGGER.info(getContext() + " received: " + message.length() + " chars");
            }
            writer.println(message.length() + "");
        }
        try {
            reader.close();
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
        writer.close();
        try {
            socket.close();
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
        LOGGER.info(getContext() + " closed");
    }
}