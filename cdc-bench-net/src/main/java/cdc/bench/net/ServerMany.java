package cdc.bench.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.bench.support.BenchSupport;
import cdc.bench.support.ChunkSupport;
import cdc.util.cli.AbstractMainSupport;

public class ServerMany {
    protected static final Logger LOGGER = LogManager.getLogger(ServerMany.class);

    private final MainArgs margs;

    public static class MainArgs extends BenchSupport.MainArgs {
        public int port;
    }

    public ServerMany(MainArgs margs) {
        this.margs = margs;
    }

    private String getContext() {
        return "[" + margs.benchName + " " + margs.port + "]";
    }

    public void run() {
        if (margs.features.isEnabled(BenchSupport.MainArgs.Feature.VERBOSE)) {
            LOGGER.info(getContext() + " starting.");
        }
        ServerSocket ssocket = null;
        try {
            ssocket = new ServerSocket(margs.port);
        } catch (final IOException e) {
            LOGGER.error(getContext() + " init failed", e);
        }
        if (ssocket != null) {
            System.out.println(getContext() + " started.");
            while (true) {
                if (margs.features.isEnabled(BenchSupport.MainArgs.Feature.VERBOSE)) {
                    LOGGER.info(getContext() + " waiting client connection");
                }
                Socket socket = null;
                try {
                    socket = ssocket.accept();
                } catch (final IOException e) {
                    LOGGER.error(getContext() + " accept failed: " + e.getMessage());
                    socket = null;
                    // e.printStackTrace();
                }
                if (socket != null) {
                    final Service service = new Service(socket);
                    service.setVerbose(margs.features.isEnabled(BenchSupport.MainArgs.Feature.VERBOSE));
                }
            }
        }
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static String PORT = "port";

        public MainSupport() {
            super(ServerMany.class, LOGGER);
        }

        @Override
        protected void addSpecificOptions(Options options) {
            ChunkSupport.addSpecificOptions(options);

            options.addOption(Option.builder()
                                    .longOpt(PORT)
                                    .desc("Port number (default 8080).")
                                    .hasArg()
                                    .build());
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();

            BenchSupport.analyze(cl, margs);

            margs.port = getValueAsInt(cl, PORT, 8080);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            final ServerMany instance = new ServerMany(margs);
            instance.run();
            return null;
        }
    }

    public static void main(String[] args) {
        final MainSupport support = new MainSupport();
        support.main(args);
    }
}