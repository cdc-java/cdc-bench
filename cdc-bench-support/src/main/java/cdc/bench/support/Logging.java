package cdc.bench.support;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Logging {
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public static void log(File file,
                           String message) {
        Writer output;
        try {
            output = new BufferedWriter(new FileWriter(file, true));
            output.append(message);
            output.write(LINE_SEPARATOR);
            output.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}