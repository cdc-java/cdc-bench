package cdc.bench.support;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import cdc.util.cli.AbstractMainSupport;

public class ChunkSupport {
    public static final String CHUNKS_SIZE = "chunks-size";
    public static final String CHUNKS_COUNT = "chunks-count";

    public static class MainArgs extends BenchSupport.MainArgs {
        public int chunksSize;
        public int chunksCount;
    }

    public static void addSpecificOptions(Options options) {
        BenchSupport.addSpecificOptions(options);

        options.addOption(Option.builder()
                                .longOpt(CHUNKS_SIZE)
                                .desc("Size of each chunk (default 1024).")
                                .hasArg()
                                .build());

        options.addOption(Option.builder()
                                .longOpt(CHUNKS_COUNT)
                                .desc("Number of chunks (default 1).")
                                .hasArg()
                                .build());
    }

    public static void analyze(CommandLine cl,
                               MainArgs margs) throws ParseException {
        BenchSupport.analyze(cl, margs);

        margs.chunksSize = AbstractMainSupport.getValueAsInt(cl, CHUNKS_SIZE, 1024);
        margs.chunksCount = AbstractMainSupport.getValueAsInt(cl, CHUNKS_COUNT, 1);
    }
}