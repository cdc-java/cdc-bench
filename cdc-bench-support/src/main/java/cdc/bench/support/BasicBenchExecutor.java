package cdc.bench.support;

public class BasicBenchExecutor extends AbstractBenchExecutor {
    private boolean cancelled = false;

    public BasicBenchExecutor(Bench bench,
                              BenchSupport.MainArgs margs) {
        super(bench,
              margs);
    }

    @Override
    protected void log(String message) {
        System.out.println(message);
    }

    public void cancel() {
        cancelled = true;
    }

    @Override
    public final boolean isCancelled() {
        return cancelled;
    }
}