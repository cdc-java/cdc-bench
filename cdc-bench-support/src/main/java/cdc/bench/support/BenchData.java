package cdc.bench.support;

import cdc.util.stats.RunningStats;

public interface BenchData {
    public enum Status {
        INITIALIZED,
        BENCHING,
        TESTING,
        SAMPLING,
        FINISHED
    }

    public Status getStatus();

    public String getBenchName();

    public int getNumberOfTests();

    public int getNumberOfSamplesPerTest();

    public int getTestsCount();

    public int getSamplesTotalCount();

    public int getSamplesTestCount();

    public RunningStats getTestSamplesTimeStats();

    public RunningStats getTestSamplesRateStats();
}