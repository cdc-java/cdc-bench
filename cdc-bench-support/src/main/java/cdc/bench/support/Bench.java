package cdc.bench.support;

import java.util.List;

/**
 * Interface implemented by Benches.
 * <p>
 * A Bench is decomposed in Tests, each Test being itself decomposed into
 * Samples.
 *
 * @author Damien Carbonne
 */
public interface Bench {
    public String getQuantityUnitName();

    public double getQuantityUnit();

    public void startBench(BenchData data,
                           List<String> values);

    public void startTest(BenchData data);

    public void startSample(BenchData data);

    public double sample(BenchData data);

    public void endSample(BenchData data);

    public void endTest(BenchData data,
                        List<String> values);

    public void endBench(BenchData data);
}