package cdc.bench.support;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.OptionEnum;

public final class BenchSupport {
    private BenchSupport() {
    }

    public static class MainArgs {
        public String benchName;
        public File logFile;
        public double delayBetweenTests;
        public int numberOfTests;
        public int numberOfSamplesPerTest;
        public final FeatureMask<Feature> features = new FeatureMask<>();

        /**
         * Enumeration of possible boolean options.
         */
        public enum Feature implements OptionEnum {
            INSERT_HEADERS("insert-headers", "Insert headers in log file."),
            VERBOSE("verbose", "Prints more messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    public static final String BENCH_NAME = "bench-name";
    public static final String LOG_FILE = "log-file";
    public static final String DELAY = "delay";
    public static final String TESTS = "tests";
    public static final String SAMPLES = "samples";

    public static void addSpecificOptions(Options options) {
        options.addOption(Option.builder()
                                .longOpt(BENCH_NAME)
                                .desc("Name of the bench instance (default ???).")
                                .hasArg()
                                .build());

        options.addOption(Option.builder()
                                .longOpt(LOG_FILE)
                                .desc("Name of the log file (default ./log.csv).")
                                .hasArg()
                                .build());

        options.addOption(Option.builder()
                                .longOpt(DELAY)
                                .desc("Delay between tests, in seconds (default 60).")
                                .hasArg()
                                .build());

        options.addOption(Option.builder()
                                .longOpt(TESTS)
                                .desc("Number of tests to run. (default -1).")
                                .hasArg()
                                .build());

        options.addOption(Option.builder()
                                .longOpt(SAMPLES)
                                .desc("Number of samples per test. (default 1).")
                                .hasArg()
                                .build());

        AbstractMainSupport.addNoArgOptions(options, MainArgs.Feature.class);
    }

    public static void analyze(CommandLine cl,
                               MainArgs margs) throws ParseException {
        margs.benchName = AbstractMainSupport.getValueAsString(cl, BENCH_NAME, "???");
        margs.delayBetweenTests = AbstractMainSupport.getValueAsDouble(cl, DELAY, 60.0);
        margs.logFile = AbstractMainSupport.getValueAsFile(cl, LOG_FILE, new File("log.csv"));
        margs.numberOfSamplesPerTest = AbstractMainSupport.getValueAsInt(cl, SAMPLES, 1);
        margs.numberOfTests = AbstractMainSupport.getValueAsInt(cl, TESTS, -1);

        AbstractMainSupport.setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);
    }
}