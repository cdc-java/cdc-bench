package cdc.bench.support.checks;

import java.util.function.Predicate;

public class StringAsIntegerPredicate implements Predicate<String> {
    private final int min;
    private final int max;

    public static final StringAsIntegerPredicate INT_INSTANCE = new StringAsIntegerPredicate();
    public static final StringAsIntegerPredicate NAT_INSTANCE = new StringAsIntegerPredicate(0, Integer.MAX_VALUE);
    public static final StringAsIntegerPredicate POS_INSTANCE = new StringAsIntegerPredicate(1, Integer.MAX_VALUE);

    public StringAsIntegerPredicate() {
        this.min = Integer.MIN_VALUE;
        this.max = Integer.MAX_VALUE;
    }

    public StringAsIntegerPredicate(int min,
                                    int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean test(String value) {
        try {
            final int x = Integer.parseInt(value);
            return x >= min && x <= max;
        } catch (final NumberFormatException e) {
            return false;
        }
    }
}