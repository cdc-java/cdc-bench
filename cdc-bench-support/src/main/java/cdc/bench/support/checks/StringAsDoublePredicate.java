package cdc.bench.support.checks;

import java.util.function.Predicate;

public class StringAsDoublePredicate implements Predicate<String> {
    private final double min;
    private final double max;

    public static final StringAsDoublePredicate INSTANCE = new StringAsDoublePredicate();
    public static final StringAsDoublePredicate NAT_INSTANCE = new StringAsDoublePredicate(0.0, Double.MAX_VALUE);

    public StringAsDoublePredicate() {
        this.min = Double.MIN_VALUE;
        this.max = Double.MAX_VALUE;
    }

    public StringAsDoublePredicate(double min,
                                   double max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean test(String value) {
        try {
            final double x = Double.parseDouble(value);
            return x >= min && x <= max;
        } catch (final NumberFormatException e) {
            return false;
        }
    }
}