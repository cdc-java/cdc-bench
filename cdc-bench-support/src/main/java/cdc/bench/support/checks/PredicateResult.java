package cdc.bench.support.checks;

public class PredicateResult {
   private final boolean applied;
   private final String message;

   public static final PredicateResult TRUE = new PredicateResult(true, null);
   public static final PredicateResult FALSE = new PredicateResult(false, null);

   public PredicateResult(boolean applied,
                          String message) {
      this.applied = applied;
      this.message = message;
   }

   public boolean isApplied() {
      return applied;
   }

   public String getMessage() {
      return message;
   }

   @Override
   public String toString() {
      return applied + " " + message;
   }
}