package cdc.bench.support.checks;

public class DoubleChecker implements Checker<String> {
   private final double min;
   private final double max;

   public static final DoubleChecker POS_INSTANCE = new DoubleChecker(0.0, Double.MAX_VALUE);

   public DoubleChecker() {
      this.min = Double.MIN_VALUE;
      this.max = Double.MAX_VALUE;
   }

   public DoubleChecker(double min,
                        double max) {
      this.min = min;
      this.max = max;
   }

   @Override
   public CheckResult check(String value) {
      try {
         final double x = Double.parseDouble(value);
         if (x < min) {
            return new CheckResult(CheckResult.Status.ERROR, "Too small double: " + x);
         } else if (x > max) {
            return new CheckResult(CheckResult.Status.ERROR, "Too large double: " + x);
         } else {
            return new CheckResult(CheckResult.Status.OK, value);
         }
      } catch (final NumberFormatException e) {
         return new CheckResult(CheckResult.Status.ERROR, "Invalid double literal: " + value);
      }
   }
}