package cdc.bench.support.checks;

import java.util.function.Predicate;

public class VerbosePredicateAdapter<E> implements Predicate<E> {
    private final VerbosePredicate<E> delegate;

    public VerbosePredicateAdapter(VerbosePredicate<E> delegate) {
        this.delegate = delegate;
    }

    @Override
    public boolean test(E value) {
        return delegate.apply(value, null).isApplied();
    }
}