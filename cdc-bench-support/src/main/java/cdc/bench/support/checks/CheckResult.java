package cdc.bench.support.checks;

public class CheckResult {
   public static enum Status {
      OK,
      INFO,
      WARNING,
      ERROR
   }

   private final Status status;
   private final String message;

   public CheckResult(Status status,
                      String message) {
      this.status = status;
      this.message = message;
   }

   public CheckResult(Status status) {
      this(status, null);
   }

   public Status getStatus() {
      return status;
   }

   public String getMessage() {
      return message;
   }

   @Override
   public String toString() {
      return status + " " + message;
   }
}