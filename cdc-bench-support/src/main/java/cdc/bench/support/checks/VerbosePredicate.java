package cdc.bench.support.checks;

public interface VerbosePredicate<E> {
   public PredicateResult apply(E value,
                                String name);
}