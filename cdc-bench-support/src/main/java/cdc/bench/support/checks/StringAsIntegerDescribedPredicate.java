package cdc.bench.support.checks;

public class StringAsIntegerDescribedPredicate implements VerbosePredicate<String> {
   private final int min;
   private final int max;

   public static final StringAsIntegerDescribedPredicate INT_INSTANCE = new StringAsIntegerDescribedPredicate();
   public static final StringAsIntegerDescribedPredicate NAT_INSTANCE = new StringAsIntegerDescribedPredicate(0, Integer.MAX_VALUE);
   public static final StringAsIntegerDescribedPredicate POS_INSTANCE = new StringAsIntegerDescribedPredicate(1, Integer.MAX_VALUE);

   public StringAsIntegerDescribedPredicate() {
      this.min = Integer.MIN_VALUE;
      this.max = Integer.MAX_VALUE;
   }

   public StringAsIntegerDescribedPredicate(int min,
                                            int max) {
      this.min = min;
      this.max = max;
   }

   @Override
   public PredicateResult apply(String value,
                                String name) {
      try {
         final int x = Integer.parseInt(value);
         if (x >= min && x <= max) {
            return PredicateResult.TRUE;
         } else {
            if (name == null) {
               return PredicateResult.FALSE;
            } else {
               return new PredicateResult(false, name + ": not in [" + min + ", " + max + "]");
            }
         }
      } catch (final NumberFormatException e) {
         if (name == null) {
            return PredicateResult.FALSE;
         } else {
            return new PredicateResult(false, name + ": invalid integer literal");
         }
      }
   }
}