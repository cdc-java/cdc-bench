package cdc.bench.support.checks;

public interface Checker<T> {
    public CheckResult check(T input);
}