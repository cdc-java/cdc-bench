package cdc.bench.support.ui.swing;

import javax.swing.JTextField;

import cdc.bench.support.checks.CheckResult;
import cdc.bench.support.checks.Checker;
import cdc.bench.support.ui.swing.InputChecker.FocusPolicy;

public class CheckedTextField extends JTextField {
    private static final long serialVersionUID = 1L;
    private Checker<String> checker;

    public CheckedTextField() {
        super();
    }

    public CheckedTextField(Checker<String> checker) {
        this();
        setChecker(checker);
    }

    public void setChecker(Checker<String> checker) {
        this.checker = checker;
        setInputVerifier(new TextInputChecker(this, FocusPolicy.NORMAL_TRANSFER, checker));
    }

    public CheckResult.Status getStatus() {
        if (checker == null) {
            return CheckResult.Status.OK;
        } else {
            return checker.check(getText()).getStatus();
        }
    }
}