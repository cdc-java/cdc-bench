package cdc.bench.support.ui.swing;

import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import cdc.bench.support.checks.CheckResult;
import cdc.bench.support.checks.Checker;

public class TextInputChecker extends InputChecker implements DocumentListener {
    private final Checker<String> checker;

    public TextInputChecker(JComponent c,
                            FocusPolicy policy,
                            Checker<String> checker) {
        super(c, policy);
        this.checker = checker;
        ((JTextComponent) c).getDocument().addDocumentListener(this);
    }

    @Override
    public CheckResult check(JComponent input) {
        if (input instanceof JFormattedTextField) {
            final JFormattedTextField w = (JFormattedTextField) input;
            return checker.check(w.getValue().toString());
        } else if (input instanceof JTextComponent) {
            return checker.check(((JTextComponent) input).getText());
        } else {
            throw new IllegalArgumentException("Unsupported class: " + input.getClass().getCanonicalName());
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        verify(c);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        verify(c);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        verify(c);
    }
}