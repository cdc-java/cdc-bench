package cdc.bench.support.ui.swing;

import java.awt.Color;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.event.DocumentListener;

import cdc.bench.support.checks.CheckResult;
import cdc.bench.support.checks.Checker;

public abstract class InputChecker extends InputVerifier implements Checker<JComponent>, DocumentListener {
    protected final JComponent c;
    private final FocusPolicy policy;
    private final Color background;
    // private final Border border;
    private final String tooltip;
    private static final Color WARNING_BG = new Color(1.0f, 1.0f, 0.6f);
    private static final Color ERROR_BG = new Color(1.0f, 0.6f, 0.6f);

    public static enum FocusPolicy {
        NORMAL_TRANSFER,
        TRANSFER_FOCUS_IF_VALID
    }

    public InputChecker(JComponent c,
                        FocusPolicy policy) {
        this.c = c;
        this.policy = policy;
        this.background = c.getBackground();
        // this.border = c.getBorder();
        this.tooltip = c.getToolTipText();
    }

    @Override
    public final boolean verify(JComponent input) {
        // System.out.println("verify()");
        assert input == c;
        final CheckResult result = check(input);
        // System.out.println(result);
        if (result == null) {
            return true;
        } else {
            switch (result.getStatus()) {
            case OK:
                c.setBackground(background);
                // c.setBorder(border);
                c.setToolTipText(tooltip);
                return true;
            case INFO:
                c.setBackground(background);
                // c.setBorder(border);
                c.setToolTipText(result.getMessage());
                return true;
            case WARNING:
                c.setBackground(WARNING_BG);
                c.setToolTipText(result.getMessage());
                return true;
            case ERROR:
            default:
                c.setBackground(ERROR_BG);
                c.setToolTipText(result.getMessage());
                return false;
            }
        }
    }

    @Override
    public boolean shouldYieldFocus(JComponent input) {
        switch (policy) {
        case TRANSFER_FOCUS_IF_VALID:
            return verify(input);
        case NORMAL_TRANSFER:
        default:
            return true;
        }
    }

    @Override
    public abstract CheckResult check(JComponent input);

}