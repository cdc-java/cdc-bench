package cdc.bench.support.ui.swing;

import javax.swing.DefaultComboBoxModel;

public class EnumComboBoxModel<E extends Enum<E>> extends DefaultComboBoxModel<E> {
    private static final long serialVersionUID = 1L;
    private final Class<E> enumClass;

    public EnumComboBoxModel(Class<E> enumClass) {
        this.enumClass = enumClass;
        for (final E value : enumClass.getEnumConstants()) {
            addElement(value);
        }
    }

    public Class<E> getEnumClass() {
        return enumClass;
    }

    public void setSelectedItem(E value) {
        super.setSelectedItem(value);
    }

    @Override
    public E getSelectedItem() {
        return enumClass.cast(super.getSelectedItem());
    }
}