package cdc.bench.support.ui.swing;

import java.util.function.Predicate;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;

public class PredicateDocumentFilter extends DocumentFilter {
    private final Predicate<String> isValid;

    public PredicateDocumentFilter(Predicate<String> isValid) {
        this.isValid = isValid;
    }

    @Override
    public void insertString(FilterBypass fb,
                             int offset,
                             String string,
                             AttributeSet attr) throws BadLocationException {

        final Document doc = fb.getDocument();
        final StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.insert(offset, string);

        if (isValid.test(sb.toString())) {
            super.insertString(fb, offset, string, attr);
        } else {
            // warn the user and don't allow the insert
        }
    }

    @Override
    public void replace(FilterBypass fb,
                        int offset,
                        int length,
                        String text,
                        AttributeSet attrs) throws BadLocationException {

        final Document doc = fb.getDocument();
        final StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.replace(offset, offset + length, text);

        if (isValid.test(sb.toString())) {
            super.replace(fb, offset, length, text, attrs);
        } else {
            // warn the user and don't allow the insert
        }

    }

    @Override
    public void remove(FilterBypass fb,
                       int offset,
                       int length) throws BadLocationException {
        final Document doc = fb.getDocument();
        final StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.delete(offset, offset + length);

        if (isValid.test(sb.toString())) {
            super.remove(fb, offset, length);
        } else {
            // warn the user and don't allow the insert
        }
    }
}