package cdc.bench.support.ui.swing;

import javax.swing.text.DocumentFilter;

import cdc.bench.support.checks.StringAsIntegerPredicate;

public class DocumentFilters {
    public static final DocumentFilter ANY_INT_FILTER = new PredicateDocumentFilter(StringAsIntegerPredicate.INT_INSTANCE);
    public static final DocumentFilter NAT_INT_FILTER = new PredicateDocumentFilter(StringAsIntegerPredicate.NAT_INSTANCE);
    public static final DocumentFilter POS_INT_FILTER = new PredicateDocumentFilter(StringAsIntegerPredicate.POS_INSTANCE);
}