package cdc.bench.support.ui.swing;

import java.util.List;

import javax.swing.SwingWorker;

import cdc.bench.support.AbstractBenchExecutor;
import cdc.bench.support.Bench;
import cdc.bench.support.BenchSupport;

public class SwingBenchExecutor extends SwingWorker<Void, String> {
    private final Executor executor;

    public SwingBenchExecutor(Bench bench,
                              BenchSupport.MainArgs margs) {
        this.executor = new Executor(bench, margs);
    }

    @Override
    protected Void doInBackground() {
        executor.run();
        return null;
    }

    @Override
    protected void process(List<String> messages) {
        if (messages != null) {
            for (final String message : messages) {
                System.out.println(message);
            }
        }
    }

    protected void publish(String message) {
        super.publish(message);
    }

    protected class Executor extends AbstractBenchExecutor {
        public Executor(Bench bench,
                        BenchSupport.MainArgs margs) {
            super(bench,
                  margs);
        }

        @Override
        protected void log(String message) {
            publish(message);
        }

        @Override
        public boolean isCancelled() {
            return SwingBenchExecutor.this.isCancelled();
        }
    }
}