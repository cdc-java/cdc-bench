package cdc.bench.support;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.SystemUtils;
import cdc.util.time.Chronometer;

/**
 * Default class that can execute the different steps of a bench.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractBenchExecutor {
    private final Logger logger = LogManager.getLogger(getClass());
    protected final Bench bench;
    protected final BenchSupport.MainArgs margs;
    private final BenchDataImpl data;

    protected AbstractBenchExecutor(Bench bench,
                                    BenchSupport.MainArgs margs) {
        this.bench = bench;
        this.margs = margs;
        this.data = new BenchDataImpl(margs.benchName,
                                      margs.numberOfTests,
                                      margs.numberOfSamplesPerTest);
    }

    protected Logger getLogger() {
        return logger;
    }

    private void log(List<String> values,
                     boolean save) {
        final StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (final String value : values) {
            if (!first) {
                sb.append(';');
            }
            sb.append(value);
            first = false;
        }
        final String message = sb.toString();
        if (save) {
            Logging.log(margs.logFile, message);
        }
        log(message);
    }

    protected abstract void log(String message);

    public abstract boolean isCancelled();

    public void run() {
        final List<String> values = new ArrayList<>();

        final Chronometer chrono = new Chronometer();

        data.startBench();
        bench.startBench(data, values);

        values.add("Time");
        values.add("Samples");
        values.add("Min Time (ms)");
        values.add("Mean Time (ms)");
        values.add("Max Time (ms)");
        values.add("Min Rate (" + bench.getQuantityUnitName() + "/s)");
        values.add("Mean Rate (" + bench.getQuantityUnitName() + "/s)");
        values.add("Max Rate (" + bench.getQuantityUnitName() + "/s)");

        log(values, margs.features.isEnabled(BenchSupport.MainArgs.Feature.INSERT_HEADERS));

        // Iterate on tests
        while (!isCancelled() && (data.getNumberOfTests() < 0 || data.getTestsCount() < data.getNumberOfTests())) {
            data.startTest();
            bench.startTest(data);

            // Iterate on samples of current test
            chrono.start();
            while (data.getSamplesTestCount() < data.getNumberOfSamplesPerTest()) {
                data.startSample();
                bench.startSample(data);
                final double quantity = bench.sample(data);
                data.endSample(quantity);
                bench.endSample(data);
            }
            chrono.suspend();
            data.endTest();
            values.clear();
            bench.endTest(data, values);

            values.add(Formatting.getCurrentTime());
            values.add(Integer.toString(data.getNumberOfSamplesPerTest()));
            values.add(Formatting.formatSecondsInms(data.getTestSamplesTimeStats().getMin()));
            values.add(Formatting.formatSecondsInms(data.getTestSamplesTimeStats().getMean()));
            values.add(Formatting.formatSecondsInms(data.getTestSamplesTimeStats().getMax()));
            values.add(Formatting.formatRate(data.getTestSamplesRateStats().getMin(), bench.getQuantityUnit()));
            values.add(Formatting.formatRate(data.getTestSamplesRateStats().getMean(), bench.getQuantityUnit()));
            values.add(Formatting.formatRate(data.getTestSamplesRateStats().getMax(), bench.getQuantityUnit()));
            log(values, true);
            if (data.getNumberOfTests() < 0 || data.getTestsCount() < data.getNumberOfTests()) {
                SystemUtils.sleepInSeconds(margs.delayBetweenTests - chrono.getElapsedSeconds());
            }
        }
        data.endBench();
        bench.endBench(data);
    }
}