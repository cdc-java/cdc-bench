package cdc.bench.support;

import cdc.util.lang.InvalidStateException;
import cdc.util.stats.RunningStats;
import cdc.util.time.Chronometer;

public class BenchDataImpl implements BenchData {
    private Status status = Status.INITIALIZED;
    private final String benchName;
    private final int numberOfTests;
    private final int numberOfSamplesPerTest;
    private int testsCount = 0;
    private int samplesTotalCount = 0;
    private int samplesTestCount = 0;
    private final RunningStats timeStats = new RunningStats();
    private final RunningStats rateStats = new RunningStats();
    private final Chronometer chrono = new Chronometer();

    public BenchDataImpl(String benchName,
                         int numberOfTests,
                         int numberOfSamplesPerTest) {
        this.benchName = benchName;
        this.numberOfTests = numberOfTests;
        this.numberOfSamplesPerTest = numberOfSamplesPerTest;
    }

    private void expects(String context,
                         Status status) {
        if (this.status != status) {
            throw new InvalidStateException("Unexpected status: " + this.status + ", expected: " + status);
        }
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public String getBenchName() {
        return benchName;
    }

    @Override
    public int getNumberOfTests() {
        return numberOfTests;
    }

    @Override
    public int getNumberOfSamplesPerTest() {
        return numberOfSamplesPerTest;
    }

    @Override
    public int getTestsCount() {
        return testsCount;
    }

    @Override
    public int getSamplesTotalCount() {
        return samplesTotalCount;
    }

    @Override
    public int getSamplesTestCount() {
        return samplesTestCount;
    }

    @Override
    public RunningStats getTestSamplesTimeStats() {
        return timeStats;
    }

    @Override
    public RunningStats getTestSamplesRateStats() {
        return rateStats;
    }

    public void startBench() {
        expects("startBench()", Status.INITIALIZED);
        status = Status.BENCHING;
        testsCount = 0;
        samplesTestCount = 0;
        samplesTotalCount = 0;
    }

    public void startTest() {
        expects("startTest()", Status.BENCHING);
        status = Status.TESTING;
        testsCount++;
        samplesTestCount = 0;
        timeStats.clear();
        rateStats.clear();
    }

    public void startSample() {
        expects("startSample()", Status.TESTING);
        status = Status.SAMPLING;
        samplesTestCount++;
        samplesTotalCount++;
        chrono.start();
    }

    public void endSample(double quantity) {
        expects("endSample()", Status.SAMPLING);

        chrono.suspend();
        final long nanos = chrono.getElapsedNanos();
        final double seconds = nanos / (double) Formatting.GIGA;
        timeStats.add(seconds);
        rateStats.add(quantity / seconds);
        status = Status.TESTING;
    }

    public void endTest() {
        expects("endTest()", Status.TESTING);
        status = Status.BENCHING;
    }

    public void endBench() {
        expects("endBench()", Status.BENCHING);
        status = Status.FINISHED;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append('[')
          .append(getBenchName())
          .append(' ')
          .append(getNumberOfTests())
          .append(' ')
          .append(getNumberOfSamplesPerTest())
          .append(' ')
          .append(getStatus())
          .append(' ')
          .append(getTestsCount())
          .append(' ')
          .append(getSamplesTotalCount())
          .append(' ')
          .append(getSamplesTestCount())
          .append(' ');
        sb.append(']');
        return sb.toString();
    }
}