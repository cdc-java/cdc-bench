# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.2.2] - 2025-01-03
### Changed
- Updated maven plugins.
- Updated dependencies:
    - cdc-io-0.53.1
    - cdc-office-0.58.1
    - cdc-perfs-0.52.1
    - cdc-ui-0.31.3
    - cdc-util-0.54.0
    - org.apache.log4j-2.24.3
    - org.junit-5.11.4
    - woodstox-7.1.0


## [0.2.1] - 2024-09-29
### Changed
- Updated maven plugins.
- Updated dependencies:
    - aalto-xml-1.3.3
    - cdc-io-0.52.1
    - cdc-office-0.57.2
    - cdc-perfs-0.52.0
    - cdc-ui-0.31.2
    - cdc-util-0.53.0
    - org.apache.log4j-2.23.1
    - org.junit-5.11.1
    - woodstox-7.0.0


## [0.2.0] - 2024-01-01
### Added
- Added `--version` option to `FileAccessBench` and `FileAccessBenchUi`.
- Added bom module. #2
- Set application icon image.

### Changed
- Updated dependencies:
    - cdc-io-0.51.0
    - cdc-office-0.52.0
    - cdc-perfs-0.51.0
    - cdc-ui-0.31.0
    - cdc-util-0.52.0
    - derby-10.16.1.1


## [0.1.0] - 2023-12-09
### Added
- Initial release